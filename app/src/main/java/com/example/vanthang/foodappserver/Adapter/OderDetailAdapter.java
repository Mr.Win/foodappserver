package com.example.vanthang.foodappserver.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vanthang.foodappserver.Models.Order;
import com.example.vanthang.foodappserver.R;

import java.util.List;

public class OderDetailAdapter extends RecyclerView.Adapter<OderDetailAdapter.ViewHolder> {
    Context context;
    List<Order>  orderList;

    public OderDetailAdapter(Context context, List<Order> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OderDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView=LayoutInflater.from(context).inflate(R.layout.order_detail_item_layout,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull OderDetailAdapter.ViewHolder viewHolder, int i) {
        viewHolder.txt_name.setText(String.format("Name : %s",orderList.get(i).getProductName()));
        viewHolder.txt_quantity.setText(String.format("Quantity : %s",orderList.get(i).getQuantity()));
        viewHolder.txt_price.setText(String.format("Price : %s",orderList.get(i).getPrice()));
        viewHolder.txt_discount.setText(String.format("Discount : %s",orderList.get(i).getDiscount()));
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_name,txt_quantity,txt_price,txt_discount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_product_name);
            txt_quantity=itemView.findViewById(R.id.txt_product_quantity);
            txt_price=itemView.findViewById(R.id.txt_product_price);
            txt_discount=itemView.findViewById(R.id.txt_product_discount);
        }
    }
}
