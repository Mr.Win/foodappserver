package com.example.vanthang.foodappserver;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.vanthang.foodappserver.Interface.ItemClickListener;
import com.example.vanthang.foodappserver.Models.Category;
import com.example.vanthang.foodappserver.Models.Food;
import com.example.vanthang.foodappserver.Utils.Common;
import com.example.vanthang.foodappserver.ViewHolder.FoodViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class FoodList extends AppCompatActivity {

    private static final int PICK_IMAGE_REQUEST = 2001;
    RecyclerView rw_food_list;
    String CategoryId="";
    FloatingActionButton    fab;
    MaterialEditText edt_name_food,edt_description_food,edt_price_food,edt_discount_food;
    Button btn_select_img,btn_upload_img;
    RelativeLayout foodlist_layout;

    FirebaseDatabase db;
    DatabaseReference foods;
    FirebaseStorage storage;
    StorageReference storageReference;
    Food newFood;

    FirebaseRecyclerAdapter<Food,FoodViewHolder> adapter;

    Uri saveImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_list);

        //init database
        db=FirebaseDatabase.getInstance();
        foods=db.getReference().child("Food");
        storage=FirebaseStorage.getInstance();
        storageReference=storage.getReference();

        //view
        fab=findViewById(R.id.fab_add_new_food);
        rw_food_list=findViewById(R.id.rw_food_list);
        foodlist_layout=findViewById(R.id.foodlist_layout);


        rw_food_list.hasFixedSize();
        rw_food_list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        if (getIntent()!=null)
            CategoryId=getIntent().getStringExtra("CategoryId");
        if (!CategoryId.isEmpty())
            loadFoodList(CategoryId);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogAddNewFood();
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImageUri = data.getData();
            btn_select_img.setText("Image Selected!");
        }
    }
    private void showDialogAddNewFood() {
        AlertDialog.Builder addNewFood = new AlertDialog.Builder(FoodList.this);
        addNewFood.setTitle("Add new Food");
        addNewFood.setMessage("Please fill full information");
        addNewFood.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View add_new_food_layout = inflater.inflate(R.layout.add_new_food_item, null);
        edt_name_food = add_new_food_layout.findViewById(R.id.edt_name_food);
        edt_description_food=add_new_food_layout.findViewById(R.id.edt_description_food);
        edt_price_food=add_new_food_layout.findViewById(R.id.edt_price_food);
        edt_discount_food=add_new_food_layout.findViewById(R.id.edt_discount_food);
        btn_select_img = add_new_food_layout.findViewById(R.id.btn_select_img);
        btn_upload_img = add_new_food_layout.findViewById(R.id.btn_upload_img);
        //event button
        btn_select_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });
        btn_upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
            }
        });
        addNewFood.setIcon(R.drawable.ic_restaurant_menu_black_24dp);
        addNewFood.setView(add_new_food_layout);
        //set Event
        addNewFood.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (newFood!=null)
                {
                    foods.push().setValue(newFood);
                    Snackbar.make(foodlist_layout,"New Food "+newFood.getName()+" was added",Snackbar.LENGTH_SHORT)
                            .setActionTextColor(Color.YELLOW).show();
                }
            }
        })
                .setPositiveButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        addNewFood.show();
    }

    private void loadFoodList(String categoryId) {
        Query query=foods.orderByChild("menuId").equalTo(categoryId);
        FirebaseRecyclerOptions<Food> options=new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query,Food.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<Food, FoodViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FoodViewHolder holder, int position, @NonNull Food model) {
                Picasso.get()
                        .load(model.getImage())
                        .into(holder.img_food_item, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {
                                Log.d("ANT",e.getMessage());
                            }
                        });
                holder.txt_food_item.setText(model.getName());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                    }
                });
            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView=LayoutInflater.from(getApplicationContext()).inflate(R.layout.food_item_layout,viewGroup,false);
                return new FoodViewHolder(itemView);
            }
        };
        adapter.notifyDataSetChanged();
        adapter.startListening();
        rw_food_list.setAdapter(adapter);
    }
    private void uploadImage() {
        if (saveImageUri != null) {
            final android.app.AlertDialog waiting = new SpotsDialog.Builder().setContext(FoodList.this).build();
            waiting.setMessage("Uploading...");
            waiting.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("image/" + imageName);
            imageFolder.putFile(saveImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            waiting.dismiss();
                            Toast.makeText(FoodList.this, "Uploaded !", Toast.LENGTH_SHORT).show();
                            imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    //set value for new Food
                                    newFood = new Food(edt_name_food.getText().toString(),
                                            uri.toString(),
                                            edt_description_food.getText().toString(),
                                            edt_price_food.getText().toString(),
                                            edt_discount_food.getText().toString(),
                                            CategoryId);
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            waiting.dismiss();
                            Log.e("ANT", e.getMessage().toString());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            waiting.setMessage("Uploaded " + progress + "%");
                        }
                    });

        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onStop() {
        if (adapter != null)
            adapter.stopListening();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.UPDATE))
        {
            showUpdateFoodDialog(adapter.getRef(item.getOrder()).getKey(),adapter.getItem(item.getOrder()));
        }
        else  if (item.getTitle().equals(Common.DELETE))
        {
            showDeleteFoodDialog(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);
    }

    private void showDeleteFoodDialog(String key) {
        foods.child(key).removeValue();
        Toast.makeText(this, "Food deleted !", Toast.LENGTH_SHORT).show();
    }

    private void showUpdateFoodDialog(final String key, final Food item) {
        AlertDialog.Builder addNewFood = new AlertDialog.Builder(FoodList.this);
        addNewFood.setTitle("Edit Food");
        addNewFood.setMessage("Please fill full information");
        addNewFood.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View add_new_food_layout = inflater.inflate(R.layout.add_new_food_item, null);
        edt_name_food = add_new_food_layout.findViewById(R.id.edt_name_food);
        edt_description_food=add_new_food_layout.findViewById(R.id.edt_description_food);
        edt_price_food=add_new_food_layout.findViewById(R.id.edt_price_food);
        edt_discount_food=add_new_food_layout.findViewById(R.id.edt_discount_food);
        btn_select_img = add_new_food_layout.findViewById(R.id.btn_select_img);
        btn_upload_img = add_new_food_layout.findViewById(R.id.btn_upload_img);

        //set default value for view
        edt_name_food.setText(item.getName());
        edt_description_food.setText(item.getDescription());
        edt_price_food.setText(item.getPrice());
        edt_discount_food.setText(item.getDiscount());

        //event button
        btn_select_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });
        btn_upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeImage(item);
            }
        });
        addNewFood.setIcon(R.drawable.ic_restaurant_menu_black_24dp);
        addNewFood.setView(add_new_food_layout);
        //set Event
        addNewFood.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                    //Update information
                    item.setName(edt_name_food.getText().toString());
                    item.setDescription(edt_description_food.getText().toString());
                    item.setPrice(edt_price_food.getText().toString());
                    item.setDiscount(edt_discount_food.getText().toString());

                    foods.child(key).setValue(item);
                    Snackbar.make(foodlist_layout,"This Food "+item.getName()+" was edited",Snackbar.LENGTH_SHORT)
                            .setActionTextColor(Color.YELLOW).show();
            }
        })
                .setPositiveButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        addNewFood.show();
    }
    private void changeImage(final Food item) {
        if (saveImageUri != null) {
            final android.app.AlertDialog waiting = new SpotsDialog.Builder().setContext(FoodList.this).build();
            waiting.setMessage("Uploading...");
            waiting.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("image/" + imageName);
            imageFolder.putFile(saveImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            waiting.dismiss();
                            Toast.makeText(FoodList.this, "Uploaded !", Toast.LENGTH_SHORT).show();
                            imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    //set value for new Menu
                                    item.setImage(uri.toString());
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            waiting.dismiss();
                            Log.e("ANT", e.getMessage().toString());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            waiting.setMessage("Uploaded " + progress + "%");
                        }
                    });

        }
    }
}
