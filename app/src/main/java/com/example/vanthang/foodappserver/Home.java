package com.example.vanthang.foodappserver;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanthang.foodappserver.Interface.ItemClickListener;
import com.example.vanthang.foodappserver.Models.Category;
import com.example.vanthang.foodappserver.Models.Token;
import com.example.vanthang.foodappserver.Utils.Common;
import com.example.vanthang.foodappserver.ViewHolder.MenuViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.UUID;

import dmax.dialog.SpotsDialog;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView txt_name_user;
    RecyclerView rw_menu;

    FirebaseDatabase db;
    FirebaseStorage storage;
    StorageReference storageReference;
    DatabaseReference menu;
    FirebaseRecyclerAdapter<Category, MenuViewHolder> adapter;
    Category newCategory;

    //add new menu layout
    MaterialEditText edt_name_new_menu;
    Button btn_select_img, btn_upload_img;

    Uri saveImageUri;
    private int PICK_IMAGE_REQUEST = 1001;
    DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //init database
        db = FirebaseDatabase.getInstance();
        menu = db.getReference("Category");
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu Management");
        toolbar.setBackgroundColor(Color.BLACK);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogAddNewMenu();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        //view
        View headerView = navigationView.getHeaderView(0);
        txt_name_user = headerView.findViewById(R.id.txt_nameuser);
        txt_name_user.setText(Common.current_User.getName());
        txt_name_user.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/NABILA.TTF"));

        rw_menu = findViewById(R.id.rw_menu);
        rw_menu.hasFixedSize();
        rw_menu.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        loadMenu();

        //Send Token
        updatetoken(FirebaseInstanceId.getInstance().getToken());
    }

    private void updatetoken(String token) {
        FirebaseDatabase db=FirebaseDatabase.getInstance();
        DatabaseReference tokens=db.getReference("Tokens");
        Token data=new Token(token,true);
        tokens.child(Common.current_User.getPhone()).setValue(data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            saveImageUri = data.getData();
            btn_select_img.setText("Image Selected!");
        }
    }

    private void showDialogAddNewMenu() {
        AlertDialog.Builder addNewFood = new AlertDialog.Builder(Home.this);
        addNewFood.setTitle("Add new Menu");
        addNewFood.setMessage("Please fill full information");
        addNewFood.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View add_new_menu_layout = inflater.inflate(R.layout.add_new_menu_item, null);
        edt_name_new_menu = add_new_menu_layout.findViewById(R.id.edt_name_new_menu);
        btn_select_img = add_new_menu_layout.findViewById(R.id.btn_select_img);
        btn_upload_img = add_new_menu_layout.findViewById(R.id.btn_upload_img);
        //event button
        btn_select_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });
        btn_upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadImage();
            }
        });
        addNewFood.setIcon(R.drawable.ic_add_black_24dp);
        addNewFood.setView(add_new_menu_layout);
        //set Event
        addNewFood.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (newCategory!=null)
                {
                    menu.push().setValue(newCategory);
                    Snackbar.make(drawer,"New Category "+newCategory.getName()+" was added",Snackbar.LENGTH_SHORT)
                            .setActionTextColor(Color.YELLOW).show();
                }
            }
        })
                .setPositiveButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        addNewFood.show();
    }

    private void uploadImage() {
        if (saveImageUri != null) {
            final android.app.AlertDialog waiting = new SpotsDialog.Builder().setContext(Home.this).build();
            waiting.setMessage("Uploading...");
            waiting.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("image/" + imageName);
            imageFolder.putFile(saveImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            waiting.dismiss();
                            Toast.makeText(Home.this, "Uploaded !", Toast.LENGTH_SHORT).show();
                            imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    //set value for new Menu
                                    newCategory = new Category(edt_name_new_menu.getText().toString(), uri.toString());
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            waiting.dismiss();
                            Log.e("ANT", e.getMessage().toString());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            waiting.setMessage("Uploaded " + progress + "%");
                        }
                    });

        }
    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private void loadMenu() {
        FirebaseRecyclerOptions<Category> options = new FirebaseRecyclerOptions.Builder<Category>()
                .setQuery(menu, Category.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<Category, MenuViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull MenuViewHolder holder, int position, @NonNull final Category model) {
                Picasso.get()
                        .load(model.getLink())
                        .into(holder.img_menu_item, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {

                            }
                        });
                holder.txt_menu_item.setText(model.getName());
                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent intent=new Intent(Home.this,FoodList.class);
                        intent.putExtra("CategoryId",adapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.menu_item_layout, viewGroup, false);
                return new MenuViewHolder(view);
            }
        };
        adapter.notifyDataSetChanged();
        adapter.startListening();
        rw_menu.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_menu) {
            // Handle the camera action
        } else if (id==R.id.nav_message){
            startActivity(new Intent(Home.this,SendMessage.class));
        }
        else if (id == R.id.nav_cart) {

        } else if (id == R.id.nav_order) {
            startActivity(new Intent(Home.this,OrderStatus.class));
        } else if (id == R.id.nav_logout) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStop() {
        if (adapter != null)
            adapter.stopListening();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null)
            adapter.startListening();
    }

    //update or delete

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle()==Common.UPDATE)
        {
            showUpdateDialog(adapter.getRef(item.getOrder()).getKey(),adapter.getItem(item.getOrder()));
        }
        else if (item.getTitle()==Common.DELETE)
        {
            showDeleteDialog(adapter.getRef(item.getOrder()).getKey());
        }
        return super.onContextItemSelected(item);
    }

    private void showDeleteDialog(String key) {
        //when delete menu ,i also will delete all food of this menu
        DatabaseReference foods=db.getReference("Food");
        Query foodOfMenu=foods.orderByChild("menuId").equalTo(key);
        foodOfMenu.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot:dataSnapshot.getChildren())
                {
                    postSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        menu.child(key).removeValue();
        Toast.makeText(this, "Item deleted !", Toast.LENGTH_SHORT).show();
    }

    private void showUpdateDialog(final String key, final Category item) {
        AlertDialog.Builder addNewFood = new AlertDialog.Builder(Home.this);
        addNewFood.setTitle("Update Menu");
        addNewFood.setMessage("Please fill full information");
        addNewFood.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View add_new_menu_layout = inflater.inflate(R.layout.add_new_menu_item, null);
        edt_name_new_menu = add_new_menu_layout.findViewById(R.id.edt_name_new_menu);
        btn_select_img = add_new_menu_layout.findViewById(R.id.btn_select_img);
        btn_upload_img = add_new_menu_layout.findViewById(R.id.btn_upload_img);
        //Set default name
        edt_name_new_menu.setText(item.getName());
        //event button
        btn_select_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });
        btn_upload_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeImage(item);
            }
        });
        addNewFood.setIcon(R.drawable.ic_add_black_24dp);
        addNewFood.setView(add_new_menu_layout);
        //set Event
        addNewFood.setNegativeButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
               item.setName(edt_name_new_menu.getText().toString());
               menu.child(key).setValue(item);
            }
        })
                .setPositiveButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        addNewFood.show();
    }

    private void changeImage(final Category item) {
        if (saveImageUri != null) {
            final android.app.AlertDialog waiting = new SpotsDialog.Builder().setContext(Home.this).build();
            waiting.setMessage("Uploading...");
            waiting.show();

            String imageName = UUID.randomUUID().toString();
            final StorageReference imageFolder = storageReference.child("image/" + imageName);
            imageFolder.putFile(saveImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            waiting.dismiss();
                            Toast.makeText(Home.this, "Uploaded !", Toast.LENGTH_SHORT).show();
                            imageFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    //set value for new Menu
                                   item.setLink(uri.toString());
                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            waiting.dismiss();
                            Log.e("ANT", e.getMessage().toString());
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            waiting.setMessage("Uploaded " + progress + "%");
                        }
                    });

        }
    }
}
