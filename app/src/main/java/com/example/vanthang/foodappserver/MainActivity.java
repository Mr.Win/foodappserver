package com.example.vanthang.foodappserver;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {
    Button btn_signin;
    TextView txt_slogan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_signin=findViewById(R.id.btn_signin);
        txt_slogan=findViewById(R.id.txt_slogan);

        txt_slogan.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/NABILA.TTF"));
        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,SignIn.class));
                finish();
            }
        });
    }

}
