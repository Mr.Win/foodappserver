package com.example.vanthang.foodappserver.Models;

public class Category {
    private String Name;
    private String Link;

    public Category() {
    }

    public Category(String name, String link) {
        Name = name;
        Link = link;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }
}
