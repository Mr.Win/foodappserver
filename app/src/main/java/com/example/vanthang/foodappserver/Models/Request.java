package com.example.vanthang.foodappserver.Models;

import java.util.List;

public class Request {
    private String Phone;
    private String Name;
    private String Address;
    private String Total;
    private String Status;
    private String Comment;
    private String LatLng;
    private List<Order> foods;

    public Request() {
    }

    public Request(String phone, String name, String address, String total, String status, String comment, String latLng, List<Order> foods) {
        Phone = phone;
        Name = name;
        Address = address;
        Total = total;
        Status = status;
        Comment = comment;
        LatLng = latLng;
        this.foods = foods;
    }

    public String getLatLng() {
        return LatLng;
    }

    public void setLatLng(String latLng) {
        LatLng = latLng;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public List<Order> getFoods() {
        return foods;
    }

    public void setFoods(List<Order> foods) {
        this.foods = foods;
    }
}
