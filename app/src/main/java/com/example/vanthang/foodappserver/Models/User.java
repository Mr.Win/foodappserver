package com.example.vanthang.foodappserver.Models;

public class User {
    private String Name;
    private String Password;
    private String Phone;
    private Boolean IsStaff;

    public User() {
    }

    public User(String name, String password, String phone, Boolean isStaff) {
        Name = name;
        Password = password;
        Phone = phone;
        IsStaff = isStaff;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public Boolean getStaff() {
        return IsStaff;
    }

    public void setStaff(Boolean staff) {
        IsStaff = staff;
    }
}
