package com.example.vanthang.foodappserver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.vanthang.foodappserver.Adapter.OderDetailAdapter;
import com.example.vanthang.foodappserver.Models.Order;
import com.example.vanthang.foodappserver.Utils.Common;

public class OrderDetail extends AppCompatActivity {
    TextView order_id,order_phone,order_total_price,order_comment,order_address;
    RecyclerView rw_listOrderDetail;

    String OrderId="";
    OderDetailAdapter oderDetailAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        //view
        order_id=findViewById(R.id.txt_order_id_detail);
        order_phone=findViewById(R.id.txt_order_phone_detail);
        order_total_price=findViewById(R.id.txt_order_total_detail);
        order_comment=findViewById(R.id.txt_order_comment_detail);
        order_address=findViewById(R.id.txt_order_address_detail);
        rw_listOrderDetail=findViewById(R.id.rw_listOrderDetail);

        rw_listOrderDetail.hasFixedSize();
        rw_listOrderDetail.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        //load data
        if (getIntent()!=null)
            OrderId=getIntent().getStringExtra("OrderId");

        //setView
        if (OrderId!=null)
            loadOrderDetail();
    }

    private void loadOrderDetail() {
        order_id.setText(new StringBuilder("#").append(OrderId));
        order_phone.setText(Common.current_Request.getPhone());
        order_total_price.setText(Common.current_Request.getTotal());
        order_comment.setText(Common.current_Request.getComment());
        order_address.setText(Common.current_Request.getAddress());

        oderDetailAdapter=new OderDetailAdapter(this,Common.current_Request.getFoods());
        oderDetailAdapter.notifyDataSetChanged();
        rw_listOrderDetail.setAdapter(oderDetailAdapter);
    }
}
