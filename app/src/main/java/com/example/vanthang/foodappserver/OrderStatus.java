package com.example.vanthang.foodappserver;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.vanthang.foodappserver.Models.DataMessage;
import com.example.vanthang.foodappserver.Models.MyResponse;
import com.example.vanthang.foodappserver.Models.Request;
import com.example.vanthang.foodappserver.Models.Token;
import com.example.vanthang.foodappserver.Remote.APIService;
import com.example.vanthang.foodappserver.Utils.Common;
import com.example.vanthang.foodappserver.ViewHolder.OrderStatusViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatus extends AppCompatActivity {
    RecyclerView rw_listOrder;
    MaterialSpinner statusSpinner;
    Request newRequest;
    APIService mService;

    FirebaseDatabase db;
    DatabaseReference orderStatus;

    FirebaseRecyclerAdapter<Request, OrderStatusViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        //init service
        mService=Common.getFCMClient();

        //init
        db = FirebaseDatabase.getInstance();
        orderStatus = db.getReference("Requests");

        rw_listOrder = findViewById(R.id.rw_listOrder);
        rw_listOrder.hasFixedSize();
        rw_listOrder.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        loadOrderStatus();
    }

    private void loadOrderStatus() {
        FirebaseRecyclerOptions<Request> options = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(orderStatus, Request.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<Request, OrderStatusViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull OrderStatusViewHolder holder, final int position, @NonNull final Request model) {
                holder.txt_order_id.setText(new StringBuilder("#").append(adapter.getRef(position).getKey()));
                holder.txt_order_status.setText(Common.convertCodeToStatus(model.getStatus()));
                holder.txt_order_address.setText(model.getAddress());
                holder.txt_order_phone.setText(model.getPhone());

                //event button
                holder.btn_edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showUpdateDialog(adapter.getRef(position).getKey(), adapter.getItem(position));
                    }
                });
                holder.btn_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDeleteDialog(adapter.getRef(position).getKey());
                    }
                });
                holder.btn_detail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(OrderStatus.this,OrderDetail.class);
                        Common.current_Request=model;
                        intent.putExtra("OrderId",adapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });
                holder.btn_direction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Common.current_Request = model;
                        startActivity(new Intent(OrderStatus.this, TrackingOrder.class));
                    }
                });
            }

            @NonNull
            @Override
            public OrderStatusViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(OrderStatus.this).inflate(R.layout.order_status_item_layout, viewGroup, false);
                return new OrderStatusViewHolder(itemView);
            }
        };
        adapter.startListening();
        adapter.notifyDataSetChanged();
        rw_listOrder.setAdapter(adapter);
    }


    private void showDeleteDialog(String key) {
        orderStatus.child(key).removeValue();
        adapter.notifyDataSetChanged();
        Toast.makeText(this, "Remove successful", Toast.LENGTH_SHORT).show();
    }

    private void showUpdateDialog(final String key, final Request item) {
        AlertDialog.Builder updateOrderStatus = new AlertDialog.Builder(OrderStatus.this);
        updateOrderStatus.setTitle("Update Order Status");
        updateOrderStatus.setMessage("Please fill full information");
        updateOrderStatus.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View update_order_layout = inflater.inflate(R.layout.update_order_layout, null);
        statusSpinner = update_order_layout.findViewById(R.id.statusSpinner);
        statusSpinner.setItems("Placed", "On my way", "Shipped");

        updateOrderStatus.setIcon(R.drawable.ic_update_black_24dp);
        updateOrderStatus.setView(update_order_layout);
        final String localKey = key;
        //set Event
        updateOrderStatus.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();


            }
        });
        updateOrderStatus.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        item.setStatus(String.valueOf(statusSpinner.getSelectedIndex()));
                        orderStatus.child(localKey).setValue(item);
                        adapter.notifyDataSetChanged();
                        sendOrderStatusToUser(localKey,item);
                    }
                });
        updateOrderStatus.show();
    }

    private void sendOrderStatusToUser(final String key,final Request item) {
        DatabaseReference tokens=FirebaseDatabase.getInstance().getReference("Tokens");
        tokens.orderByKey().equalTo(item.getPhone())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren())
                        {
                            Token token=postSnapshot.getValue(Token.class);

                            //Make raw payload
//                            Notification notification=new Notification("Mr.Win","Your order"+key+"was updated !");
//                            Sender sender=new Sender(token.getToken(),notification);
                            Map<String,String> dataSend=new HashMap<>();
                            dataSend.put("title","Mr.Win");
                            dataSend.put("message","Your order"+key+"was updated !");
                            DataMessage dataMessage=new DataMessage(token.getToken(),dataSend);

                            mService.sendNotification(dataMessage).enqueue(new Callback<MyResponse>() {
                                @Override
                                public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                    if (response.code()==200)
                                    {
                                        if (response.body().success==1)
                                        {
                                            Toast.makeText(OrderStatus.this, "Order was updated !", Toast.LENGTH_SHORT).show();
                                        }else {
                                            Toast.makeText(OrderStatus.this, "Order was updated but failed !", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<MyResponse> call, Throwable t) {
                                    Log.d("Mr.Win",t.getMessage());
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
}
