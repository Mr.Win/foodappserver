package com.example.vanthang.foodappserver.Remote;


import com.example.vanthang.foodappserver.Models.DataMessage;
import com.example.vanthang.foodappserver.Models.MyResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAM3cvtqA:APA91bEBjiiddJbeBwbiX_uGBzrC4nb08B3HWwd6POHrUCRmGyZ6Jfnb9yx0gW1U5wZdHCq4Lg6xqcjR0LRsTrnILHTXr5cHwWLbqHu4Ta0ZnJS3nazhqOMpkeJs9ZCv877MOI013ztN"
    })
    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body DataMessage body);
}
