package com.example.vanthang.foodappserver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.vanthang.foodappserver.Models.DataMessage;
import com.example.vanthang.foodappserver.Models.MyResponse;
import com.example.vanthang.foodappserver.Remote.APIService;
import com.example.vanthang.foodappserver.Utils.Common;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendMessage extends AppCompatActivity {

    MaterialEditText edt_title,edt_message;
    Button btn_send;

    APIService mService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        mService= Common.getFCMClient();

        //view
        edt_title=findViewById(R.id.edt_title);
        edt_message=findViewById(R.id.edt_message);
        btn_send=findViewById(R.id.btn_send);

        //event
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //create message
//                Notification notification=new Notification(edt_title.getText().toString(),edt_message.getText().toString());
//
//                Sender toTopic=new Sender();
//                toTopic.to=new StringBuilder("/topics/").append(Common.topicName).toString();
//                toTopic.notification=notification;
                Map<String,String> dataSend=new HashMap<>();
                dataSend.put("title",edt_title.getText().toString());
                dataSend.put("message",edt_message.getText().toString());
                String to=new StringBuilder("/topics/").append(Common.topicName).toString();
                DataMessage dataMessage=new DataMessage(to,dataSend);

                mService.sendNotification(dataMessage)
                        .enqueue(new Callback<MyResponse>() {
                            @Override
                            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                                if (response.isSuccessful()){
                                    Toast.makeText(SendMessage.this, "Message Sent", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<MyResponse> call, Throwable t) {
                                Toast.makeText(SendMessage.this, ""+t.getMessage().toString(), Toast.LENGTH_SHORT).show();

                            }
                        });
            }
        });
    }
}
