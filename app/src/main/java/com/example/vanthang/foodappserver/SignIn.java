package com.example.vanthang.foodappserver;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.vanthang.foodappserver.Models.User;
import com.example.vanthang.foodappserver.Utils.Common;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignIn extends AppCompatActivity {
    MaterialEditText edt_phone, edt_password;
    Button btn_signin;

    FirebaseDatabase db;
    DatabaseReference admin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Arkhip_font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_sign_in);

        //init database
        db=FirebaseDatabase.getInstance();
        admin=db.getReference("Users");

        //view
        edt_phone=findViewById(R.id.edt_phone);
        edt_password=findViewById(R.id.edt_password);
        btn_signin=findViewById(R.id.btn_signin);

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInUser(edt_phone.getText().toString(),edt_password.getText().toString());
            }
        });
    }

    private void signInUser(final String phone, final String password) {

        final AlertDialog waitingDialog=new SpotsDialog.Builder().setContext(SignIn.this).build();
        waitingDialog.setMessage("Please waiting");
        waitingDialog.show();
        admin.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(phone).exists())
                {
                    waitingDialog.dismiss();
                    User admin=dataSnapshot.child(phone).getValue(User.class);
                    if (admin.getStaff()==true)
                    {
                        if (password.equals(admin.getPassword()))
                        {
                            //Login into app
                            Common.current_User=admin;
                            startActivity(new Intent(SignIn.this,Home.class));
                            Toast.makeText(SignIn.this, "Sign In Successful", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        else
                            Toast.makeText(SignIn.this, "Password is wrong!", Toast.LENGTH_SHORT).show();
                    }else
                        Toast.makeText(SignIn.this, "Please login with staff account", Toast.LENGTH_SHORT).show();
                }else
                {
                    waitingDialog.dismiss();
                    Toast.makeText(SignIn.this, "User not exists!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
