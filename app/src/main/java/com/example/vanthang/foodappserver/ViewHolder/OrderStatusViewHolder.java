package com.example.vanthang.foodappserver.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.vanthang.foodappserver.Interface.ItemClickListener;
import com.example.vanthang.foodappserver.R;

public class OrderStatusViewHolder extends RecyclerView.ViewHolder {
    public TextView txt_order_id,txt_order_status,txt_order_phone,txt_order_address;
    public Button btn_edit,btn_remove,btn_detail,btn_direction;


    public OrderStatusViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_order_id=itemView.findViewById(R.id.txt_order_id);
        txt_order_status=itemView.findViewById(R.id.txt_order_status);
        txt_order_phone=itemView.findViewById(R.id.txt_order_phone);
        txt_order_address=itemView.findViewById(R.id.txt_order_address);
        btn_edit=itemView.findViewById(R.id.btn_edit);
        btn_remove=itemView.findViewById(R.id.btn_remove);
        btn_detail=itemView.findViewById(R.id.btn_detail);
        btn_direction=itemView.findViewById(R.id.btn_direction);

    }

}
